"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _crypto = require("crypto");

var _crypto2 = _interopRequireDefault(_crypto);

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _ws = require("ws");

var _ws2 = _interopRequireDefault(_ws);

var _bodyParser = require("body-parser");

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _Block = require("./model/Block");

var _Block2 = _interopRequireDefault(_Block);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var http_port = process.env.HTTP_PORT || 3001;
var p2p_port = process.env.P2P_PORT || 6001;
var initialPeers = process.env.PEERS ? process.env.PEERS.split(',') : [];

var sockets = [];
var MessageType = {
  QUERY_LATEST: 0,
  QUERY_ALL: 1,
  RESPONSE_BLOCKCHAIN: 2
};

var getGenesisBlock = function getGenesisBlock() {
  return new _Block2.default(0, "0", 1465154705, "my genesis block!!", "816534932c2b7154836da6afc367695e6337db8a921823784c14378abed4f7d7");
};

var blockchain = [getGenesisBlock()];

var initHttpServer = function initHttpServer() {
  var app = (0, _express2.default)();
  app.use(_bodyParser2.default.json());

  app.get('/blocks', function (req, res) {
    return res.send(JSON.stringify(blockchain));
  });
  app.post('/mineBlock', function (req, res) {
    var newBlock = generateNextBlock(req.body.data);
    addBlock(newBlock);
    broadcast(responseLatestMsg());
    console.log('block added: ' + JSON.stringify(newBlock));
    res.send();
  });
  app.get('/peers', function (req, res) {
    res.send(sockets.map(function (s) {
      return s._socket.remoteAddress + ':' + s._socket.remotePort;
    }));
  });
  app.post('/addPeer', function (req, res) {
    connectToPeers([req.body.peer]);
    res.send();
  });
  app.listen(http_port, function () {
    return console.log('Listening http on port: ' + http_port);
  });
};

var initP2PServer = function initP2PServer() {
  var server = new _ws2.default.Server({ port: p2p_port });
  server.on('connection', function (ws) {
    return initConnection(ws);
  });
  console.log('listening websocket p2p port on: ' + p2p_port);
};

var initConnection = function initConnection(ws) {
  sockets.push(ws);
  initMessageHandler(ws);
  initErrorHandler(ws);
  write(ws, queryChainLengthMsg());
};

var initMessageHandler = function initMessageHandler(ws) {
  ws.on('message', function (data) {
    var message = JSON.parse(data);
    console.log('Received message' + JSON.stringify(message));
    switch (message.type) {
      case MessageType.QUERY_LATEST:
        write(ws, responseLatestMsg());
        break;
      case MessageType.QUERY_ALL:
        write(ws, responseChainMsg());
        break;
      case MessageType.RESPONSE_BLOCKCHAIN:
        handleBlockchainResponse(message);
        break;
    }
  });
};

var initErrorHandler = function initErrorHandler(ws) {
  var closeConnection = function closeConnection(ws) {
    console.log('connection failed to peer: ' + ws.url);
    sockets.splice(sockets.indexOf(ws), 1);
  };
  ws.on('close', function () {
    return closeConnection(ws);
  });
  ws.on('error', function () {
    return closeConnection(ws);
  });
};

var generateNextBlock = function generateNextBlock(blockData) {
  var previousBlock = getLatestBlock();
  var nextIndex = previousBlock.index + 1;
  var nextTimestamp = new Date().getTime() / 1000;
  var nextHash = calculateHash(nextIndex, previousBlock.hash, nextTimestamp, blockData);
  return new _Block2.default(nextIndex, previousBlock.hash, nextTimestamp, blockData, nextHash);
};

var calculateHashForBlock = function calculateHashForBlock(block) {
  return calculateHash(block.index, block.previousHash, block.timestamp, block.data);
};

var calculateHash = function calculateHash(index, previousHash, timestamp, data) {
  return _crypto2.default.createHash('sha256').update(index + previousHash + timestamp + data, 'utf8').digest('hex');
};

var addBlock = function addBlock(newBlock) {
  if (isValidNewBlock(newBlock, getLatestBlock())) {
    blockchain.push(newBlock);
  } else {
    console.log('addBlock fail');
  }
};

var isValidNewBlock = function isValidNewBlock(newBlock, previousBlock) {
  if (previousBlock.index + 1 !== newBlock.index) {
    console.log('invalid index');
    return false;
  } else if (previousBlock.hash !== newBlock.previousHash) {
    console.log('invalid previoushash');
    return false;
  } else if (calculateHashForBlock(newBlock) !== newBlock.hash) {
    console.log(_typeof(newBlock.hash) + ' ' + _typeof(calculateHashForBlock(newBlock)));
    console.log('invalid hash: ' + calculateHashForBlock(newBlock) + ' ' + newBlock.hash);
    return false;
  }
  return true;
};

var connectToPeers = function connectToPeers(newPeers) {
  newPeers.forEach(function (peer) {
    var ws = new _ws2.default(peer);
    ws.on('open', function () {
      return initConnection(ws);
    });
    ws.on('error', function () {
      console.log('connection failed');
    });
  });
};

var handleBlockchainResponse = function handleBlockchainResponse(message) {
  var receivedBlocks = JSON.parse(message.data).sort(function (b1, b2) {
    return b1.index - b2.index;
  });
  var latestBlockReceived = receivedBlocks[receivedBlocks.length - 1];
  var latestBlockHeld = getLatestBlock();
  if (latestBlockReceived.index > latestBlockHeld.index) {
    console.log('blockchain possibly behind. We got: ' + latestBlockHeld.index + ' Peer got: ' + latestBlockReceived.index);
    if (latestBlockHeld.hash === latestBlockReceived.previousHash) {
      console.log("We can append the received block to our chain");
      blockchain.push(latestBlockReceived);
      broadcast(responseLatestMsg());
    } else if (receivedBlocks.length === 1) {
      console.log("We have to query the chain from our peer");
      broadcast(queryAllMsg());
    } else {
      console.log("Received blockchain is longer than current blockchain");
      replaceChain(receivedBlocks);
    }
  } else {
    console.log('received blockchain is not longer than received blockchain. Do nothing');
  }
};

var replaceChain = function replaceChain(newBlocks) {
  if (isValidChain(newBlocks) && newBlocks.length > blockchain.length) {
    console.log('Received blockchain is valid. Replacing current blockchain with received blockchain');
    blockchain = newBlocks;
    broadcast(responseLatestMsg());
  } else {
    console.log('Received blockchain invalid');
  }
};

var isValidChain = function isValidChain(blockchainToValidate) {
  if (JSON.stringify(blockchainToValidate[0]) !== JSON.stringify(getGenesisBlock())) {
    return false;
  }
  var tempBlocks = [blockchainToValidate[0]];
  for (var i = 1; i < blockchainToValidate.length; i++) {
    if (isValidNewBlock(blockchainToValidate[i], tempBlocks[i - 1])) {
      tempBlocks.push(blockchainToValidate[i]);
    } else {
      return false;
    }
  }
  return true;
};

var getLatestBlock = function getLatestBlock() {
  return blockchain[blockchain.length - 1];
};
var queryChainLengthMsg = function queryChainLengthMsg() {
  return { 'type': MessageType.QUERY_LATEST };
};
var queryAllMsg = function queryAllMsg() {
  return { 'type': MessageType.QUERY_ALL };
};
var responseChainMsg = function responseChainMsg() {
  return {
    'type': MessageType.RESPONSE_BLOCKCHAIN, 'data': JSON.stringify(blockchain)
  };
};
var responseLatestMsg = function responseLatestMsg() {
  return {
    'type': MessageType.RESPONSE_BLOCKCHAIN,
    'data': JSON.stringify([getLatestBlock()])
  };
};

var write = function write(ws, message) {
  return ws.send(JSON.stringify(message));
};
var broadcast = function broadcast(message) {
  return sockets.forEach(function (socket) {
    return write(socket, message);
  });
};

connectToPeers(initialPeers);
initHttpServer();
initP2PServer();
//# sourceMappingURL=server.js.map